package generics.pair;

public class Pair {

    private final String first;
    private final Integer second;

    public Pair(String first, Integer second) {
        this.first = first;
        this.second = second;
    }

    public String getFirst() {
        return first;
    }

    public Integer getSecond() {
        return second;
    }

}
